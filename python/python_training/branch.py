a = 5

if a == 5:
    print("hello")
else:
    print("hi!")

b = 4
if b < 3:
     print("hello")
elif b < 5:
     print("hi!")
else:
     print("yeah!")

time = 15

if time > 5 and  time < 12:
    print("Good morning")
elif time >= 12 and time < 18:
    print("Good afternoon")
else:
    print("Good night")