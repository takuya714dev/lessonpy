a = {"taro":1985,"hanako":1986}
print(a)

b = a["taro"]
print(b)

a["jiro"] = 1988
print(a)

a["hanako"] = 1990
print(a)

c = {1:1991, 2:1992}
print(c)

d = c[2]
print(d)