import pandas as pd
from pandas import Series

obj = Series([3,6,9,12])

print(obj)

print(obj.values)
print(obj.index)

ww2_cos = Series([8700000,4300000,3000000,2100000,400000], index=["USSR","Germany","China","Japan","USA"])

print(ww2_cos)

print(ww2_cos["USA"])
print(ww2_cos[ww2_cos > 4000000])

print("USSR" in ww2_cos)

ww2_dict = ww2_cos.to_dict()

ww2_Serias = Series(ww2_dict)

print(ww2_Serias)

countries = ["China","Germany","Japan","USA","USSR","Argetina"]
obj2 = Series(ww2_dict,index=countries)

print(obj2)

print(pd.isnull(obj2))#nullか調べる
print(pd.notnull(obj2))#null以外か調べる


print(ww2_Serias + obj2)

obj2.name = "第２次世界大戦の死傷者"
print(obj2)

obj2.index.name = "countries"

print(obj2)