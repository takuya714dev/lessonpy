import numpy as np
arr = np.arange(9).reshape(3,3)#reshapeメソッドはarrayの構造を変化させることができる
print(arr)
print(arr.T)# T と書くだけで（Tは転置の意）簡単に転置行列が生成できる

print(arr.transpose((1,0)))#0は行1は列を表しており、←は(行、列)が(1,0) = (列、行)になっているため転置する

print(arr.swapaxes(1,0))#swapaxesは２つの軸を転換させる際に使用する

print(np.dot(arr.T, arr))#dotメソッドは行列の掛け算ができる
print(arr.T * arr)#こっちは単純に同じ要素番号を掛け合わせただけ

arr3d = np.arange(12).reshape(3,2,2)
print(arr3d)

print(arr3d.transpose((0,2,1)))

