'''前の問題よりコンパクトに血液型、年齢、生年月日、学年、ゼミが表示できるようにしなさい
ただし、血液型、年齢、生年月日はMy_Dataクラスにいれて
学年、学籍番号、ゼミはOtani_studentクラスに入れなさい'''
from lesson_class007c import Otani_Student

a = Otani_Student("junior","9876543","datascience","B","36","1678/10/05")
a.Display()
