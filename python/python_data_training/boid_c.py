birdNumber = 10 #birdの数
centerPullFactor = 300 #群れの中心に引き寄せられる力

#動く個体のパラメータ
class Bird():
    def __init__(self, x, y, vx, vy, id, others):
        self.X = x
        self.Y = y #birdインスタンスの現在の位置
        self.VX = vx
        self.VY = vy #移動量
        self.ID = id #識別id
        self.OTHERS = others #他の個体達


class Attraction(Bird):
    def __init__(self):