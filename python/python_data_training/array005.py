import numpy as np
arr = np.arange(5)

print(arr)

np.save("my_array",arr)

arr = np.arange(10)

print(arr)
arr1 = np.load("my_array.npy")

print(arr1)

np.savez("ziparray.npz",x=arr,y=arr1)

archive_array = np.load("ziparray.npz")
print(archive_array)

print(archive_array["x"])#こんな感じで保存したデータ呼び出すことができる

np.savetxt("my_test_text.txt",arr,delimiter=",")

arr = np.loadtxt("my_test_text.txt",delimiter=",")

print(arr)