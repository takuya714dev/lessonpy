from layer_bf import MultiLayer

back_price = 1
apple = 100
apple_num = 2
tax = 1.1
multi_apple_layer = MultiLayer()
multi_tax_layer = MultiLayer()
#forward
#1層目
apple_price = multi_apple_layer.forward(apple, apple_num)
#2層目
price =multi_tax_layer.forward(apple_price, tax)

#backward
#1層目
back_apple_price, back_tax = multi_tax_layer.backward(back_price)
#2層目
back_apple, back_apple_num =multi_apple_layer.backward(back_apple_price)

print("price:", int(back_price))
print("Apple:", back_apple)
print("Apple_num:", int(back_apple_num))
print("Tax:", back_tax)

