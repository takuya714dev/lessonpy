import pandas as pd
import numpy as np
from pandas import Series, DataFrame
from numpy.random import randn
# my_ser = Series([1,2,3,4], index = ["A","B","C","D"])
# print(my_ser)
#
# my_index = my_ser.index
# print(my_index)
# print(my_index[2])
# print(my_index[2:])

# ser1 = Series([1,2,3,4],index = ["A","B","C","D"])
# print(ser1)
#
# ser2 = ser1.reindex(["A","B","C","D","E","F"])
# print(ser2)
#
# ser3 = Series(["USA","MEXICO","CANADA"],index = [0,5,10])
#
# print(ser3)
# ser4 = ser3.reindex(range(15), method="ffill")
# print(ser4)
#
# rame = DataFrame(randn(25).reshape((5,5)), index=["A","B","D","E","F"], columns=["col1","col2","col3","col4","col5"])
# print(rame)
# new_index = ["A","B","C","D","E","F"]
#
# ser1 = Series(np.arange(3), index=["a","b","c"])
# print(ser1)
# ser1.drop(0)
# print(ser1)

# ser1 = Series(np.arange(3), index=["A","B","C"])
# ser1 = 2 * ser1
# print(ser1)
# print(ser1[1],ser1["B"],ser1[["A","B"]],)

# class Prop:
#     def __init__(self):
#         self.__x = 0
#     def getx(self):
#         return self.__x
#     def setx(self,x):
#         self.__x = x
#     x = property(getx, setx)
#
#
# class TestGetattr:
#     def __init__(self):
#         self.x = 0
#     def __getattr__(self, item):
#         self.ITEM = item
#         print(self.ITEM)
#
# a = TestGetattr()
# print(a.h)

# ser1 = Series(range(3), index=["C","A","B"])
# print(ser1)
# print(ser1.sort_index())
# ser2 = Series(randn(10))
# print(ser2)
# print(ser2.rank())
# ser2.sort_values(inplace=True)
# print(ser2)
# print(ser2.rank())

# arr = np.array([[1,2,np.nan],[np.nan,3,4]])
# print(arr)
#
# dframe1 = DataFrame(arr, index=["A","B"],columns=["one","two","three"])
# print(dframe1)
# print(dframe1.cumsum())
# print(dframe1.describe())
# import pandas.io.d
data = Series([1,2,3,4,None,6,7,8,9])
print(data.isnull())

