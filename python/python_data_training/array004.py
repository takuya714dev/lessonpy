import numpy as np
import matplotlib.pyplot as plt

points = np.arange(-5,5,0.01)

dx,dy = np.meshgrid(points,points)#meshgridで格子点を生成

print(dx)
print(dy)

#plt.imshow(dx)
#plt.show()

#plt.imshow(dy)
#plt.show()

z = (np.sin(dx) + np.sin(dy))

#plt.imshow(z)
#plt.colorbar()
#plt.title("plot for sinx - siny")
#plt.show()

A = np.array([1,2,3,4])
B = np.array([1000,2000,3000,4000])

condition = np.array([True,True,False,False])

#こういったやつをリスト内包表記というらしい、pythonでは日常的に使用するみたい、確かになれたら便利そうな予感
answer = [(a if cond else b) for a,b, cond in zip(A,B,condition)]

print(answer)

answer2 = np.where(condition,A,B)

print(answer2)

from numpy.random import randn

arr = randn(5,5)
print(arr)

print(np.where(arr < 0, 0, arr))

arr = np.array([[1,2,3],[4,5,6],[7,8,9]])
print(arr)

print(arr.sum())
print(arr.sum(0))#sumメソッドは行だけとか列だけとかでも計算ができる

print(arr.mean())#平均
print(arr.std())#標準偏差
print(arr.var())#分散

bool_arr = np.array([True,False,True])
print(bool_arr)

print(bool_arr.any())#anyは一つでもtrueが含まれていればtrueを返す
print(bool_arr.all())#allはすべてがtrueならばtrueを返す

arr = randn(5)
arr.sort()
print(arr)

countries = np.array(["France","Japan","USA","Russia","USA","Mexico","Japan"])
print(np.unique(countries))#アレイ内の被っている要素を消す

print(np.in1d(["France","USA","Sweden"],countries))#明示された要素がアレイ内の要素と合致しているかを返す