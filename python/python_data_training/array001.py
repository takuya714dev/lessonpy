import numpy as np

my_list1 = [1, 2, 3, 4]
my_array1 = np.array(my_list1)

print(my_array1)

my_list2 = [11, 22, 33, 44]

my_lists = [my_list1, my_list2]

print(my_lists)

my_array2 = np.array(my_lists)
print(my_array2)

#shapeは行と列の数がタプルで格納されている
print(my_array2.shape)

#dtypeはデータ型が格納されている
print(my_array2.dtype)

#zerosは引数の要素数を持ったすべての要素が０の
my_zeros = np.zeros(5)
print(my_zeros)

#zerosはfloat型になる模様
print(my_zeros.dtype)

#onesというのもあるみたい、中身はzerosの1バージョン
my_ones = np.ones((5,5))
print(my_ones)

#上二つと同じようにemptyというのもある、中身は空

print(np.empty((2,4)))

#eyeは単位行列が生成できる
print(np.eye(3))

#arrangeはレンジと数字の幅を決めることができる
print(np.arange(5,50,2))


###### レクチャー8 #####
print(5/2)#python3だとint型同士割り算等の計算でもちゃんとfloat型に変換される

arr1 = np.array([[1,2,3,4],[8,9,10,11]])
print(arr1 * arr1)

print(arr1 - arr1)

#↓みたいに数字で計算もできる
print(1/arr1)

print(arr1 ** 3)

##### レクチャー9 #####

arr = np.arange(0,11)

print(arr)
print(arr[8])
print(arr[1:5])#indexの1~5までを表示(スライス等と呼ばれる

arr[1:5] = 100#こんな感じで代入もできちゃう
print(arr)

arr[:] = 99# : だけだとすべての要素を意味している
print(arr)

arr_copy = arr.copy()#コピーもできちゃう
print(arr_copy)

arr_2d = np.array([[5,10,15],[20,25,30],[35,40,45]])
print(arr_2d)

print(arr_2d[1][0])

print(arr_2d[:2,1:])#こんな感じで配列の好きなところを素破抜くこともできる
print(arr_2d[1,:])

arr2d = np.zeros((10,10))
print(arr2d)

arr_length = arr2d.shape[1]
for i in range(arr_length):
    arr2d[i] = i
print(arr2d)
