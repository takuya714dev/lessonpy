# def AND(x1, x2):
#     w1, w2, theta = 0.5, 0.5, 0.7
#     tmp = x1 * w1 + x2 * w2
#     if tmp <= theta:
#         return 0
#     elif tmp >= theta:
#         return 1
#
# print(AND(0,0))
# print(AND(1,0))
# print(AND(0,1))
# print(AND(1,1))
# import numpy as np
#
# def AND(x1, x2):
#     x = np.array([x1, x2])
#     w = np.array([0.5, 0.5])
#     b = -0.7
#     tmp = np.sum(w * x) + b
#     if tmp <= 0:
#         return 0
#     else:
#         return 1
#
# def NAND(x1, x2):
#     x = np.array([x1, x2])
#     w = np.array([-0.5, -0.5])
#     b = 0.7
#     tmp = np.sum(w * x) + b
#     if tmp <= 0:
#         return 0
#     else:
#         return 1
#
# def OR(x1, x2):
#     x = np.array([x1, x2])
#     w = np.array([0.5, 0.5])
#     b = -0.2
#     tmp = np.sum(w * x) + b
#     if tmp <= 0:
#         return 0
#     else:
#         return 1
#
# def XOR(x1, x2):
#     s1 = NAND(x1, x2)
#     s2 = OR(x1, x2)
#     y = AND(s1, s2)
#     return y
#
#
# print(AND(0,0))
# print(AND(1,0))
# print(AND(0,1))
# print(AND(1,1))
# print("\n")
# print(NAND(0,0))
# print(NAND(1,0))
# print(NAND(0,1))
# print(NAND(1,1))
# print("\n")
# print(OR(0,0))
# print(OR(1,0))
# print(OR(0,1))
# print(OR(1,1))
# print("\n")
# print(XOR(0,0))
# print(XOR(1,0))
# print(XOR(0,1))
# print(XOR(1,1))
# import numpy as np
# import matplotlib.pyplot as plt
#
# def step_function(x):
#     return np.array(x > 0, dtype=np.int)#booleanをintにすることで1,0を吐き出すことができている
#
# def sigmoid_function(x):
#     return 1/(1 + np.exp(-x))
# x = np.arange(-5.0, 5.0, 0.1)
# y = step_function(x)
# plt.plot(x,y)
# ys = sigmoid_function(x)
# plt.plot(x,ys)
# plt.show()
import numpy as np

def identify_function(x):
    return x

def softmax(a):
    c = np.max(a)
    exp_a = np.exp(a - c)#オーバーフロー対策
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a
    return y


def sigmoid_function(x):
    return 1/(1 + np.exp(-x))

def init_network():
    network = {}
    network["w1"] = np.array([[0.1,0.3,0.5],[0.2,0.4,0.6]])
    network["b1"] = np.array([0.1,0.2,0.3])
    network["w2"] = np.array([[0.1,0.4],[0.2,0.5],[0.3,0.6]])
    network["b2"] = np.array([0.1,0.2])
    network["w3"] = np.array([[0.1,0.3],[0.2,0.4]])
    network["b3"] = np.array([0.1,0.2])
    return network

def forward(network, x):
    w1,w2,w3 = network["w1"],network["w2"],network["w3"]
    b1,b2,b3 = network["b1"],network["b2"],network["b3"]

    a1 = np.dot(x,w1) + b1
    z1 = sigmoid_function(a1)
    a2 = np.dot(z1,w2) + b2
    z2 = sigmoid_function(a2)
    a3 = np.dot(z2,w3) + b3
    y = identify_function(a3)
    return y

network = init_network()
x = np.array([1.0,0.5])
y = forward(network, x)
print(y)
