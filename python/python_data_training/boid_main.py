'''アウトプット初級編として、boidプログラムを製作してみる
勉強のために、通常より細かくコメントアウトをすることと、classを使用して
mainはできるだけコンパクトに制作することを心掛ける
ちなみにboidとは以下のルールの通りに動作するものことである
・多くの個体がいる方向に動く
・多くの個体と近づきすぎたらぶつからないように離れる
・近くの者同士で動くスピードを合わせる'''

# -*- coding: UTF-8 -*-
import math
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# 更新する内容
def _update_plot(i, fig, im):
    # 前回のフレーム内容を一旦削除
    if len(im) > 0:
        im[0].remove()
        im.pop()

    im.append(plt.scatter(100,100))#個体を描画

fig =  plt.figure()

# グラフを中央に表示
ax = fig.add_subplot(1,1,1)

# グラフの目盛範囲設定
ax.set_xlim([0, 1000])
ax.set_ylim([0, 1000])

im = [] # フレーム更新の際に前回のプロットを削除するために用意

# アニメーション作成
ani = animation.FuncAnimation(fig, _update_plot, fargs = (fig, im),
        frames = 360, interval = 1)

# 表示
plt.show()